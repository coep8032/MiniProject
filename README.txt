Title:
	Infix Calculator with GTK library graphics.
		(Numbers range upto double)
Name : Prithviraj Kisan Jadhav
MIS : 111608032

* Graphical Calculator available with operations displayed on the calculator.
* Graphics created in an XML file which is imported in the calc.c using GtkBuilder.
* The Buttons do their respective work,"MEM" button specifically, saves the answer from the output bar and can retrieved until the output bar 	is empty. When pressed again, output being non-empty, it refreshes the value to this new answer.
* "C" clears single entry, while "CLR" clears everything. Start new calculation by pressing this.
* sin, cos, tan accepts angles in radian.
* log is to the base e.
* "Revaluating" being printed on terminal implies the that whether the expression was truly Errorless.
* Entry can either be done by buttons on calculator or keyboard at a single time.
