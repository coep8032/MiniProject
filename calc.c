#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include"stack.h"

enum states{START, OP, STOP, OPERAND, END, ERROR, ERR, OPERATOR};
stack nums;
stackc sym;

/*
	textBox is struct of two GtkEntries : Input and Output
	Comes handy while displaying and retrieving the string
*/
typedef struct textBox{
	GtkEntry *e1, *e2;
}textBox;

/*
	token required to return character type while tokenizing input 		string
*/

typedef struct token{
	int type;
	union data {
		double num;
		char op;
	}data;
}token;

/*
	All Calculations begin from here.
*/

double factorial (double p){
	double prod = 1;
	if(fmod(p, 1) != 0)
		return NAN;	
	else if(p < 0)
		return INT_MIN;
	else if(p == 0){
		return 1;
	}else{
		while(1){
			if(p == 0)
				break;
			prod = prod * p;
			p--;
		}
	}
	return prod;

}
void process(char temp)   {
    double res, x, y;
    if(isempty(&nums))
	return;
    if(temp != '(' && temp != ')')	{
    	x = pop(&nums);
    	if(temp != '!' && temp != 's' && temp != 'c' && temp != 't' && temp != 'l' && temp != 'i' && temp != 'q'){
		if(isempty(&nums)){	
			push(&nums, INT_MIN);
			return;
		}
	}
    	y = pop(&nums);
    }else
    		return;
    switch(temp)    {
	     case 'q':
		push(&nums, y);
		res = sqrt(x);
		push(&nums, res);
		break;
	     case 's':
		push(&nums, y);
		res = sin(x);
		push(&nums, res);
		break;
	     case 'c':
		push(&nums, y);
		res = cos(x);
		push(&nums, res);
		break;
	     case 't':
		push(&nums, y);
		res = tan(x);
		push(&nums, res);
		break;
	     case 'l':
		push(&nums, y);
		res = log(x);
		push(&nums, res);
		break;
             case 'i':
		push(&nums, y);
		res = pow(x, -1);
		push(&nums, res);
		break;
	    case '!':
	    	push(&nums, y);
		res = factorial(x);
		push(&nums, res);
		break;
	    case '^':
                res =  pow(y,x);
                push(&nums, res);
                break;
            case '+':
                res =  y + x;
                push(&nums, res);
                break;
            case '-':
                res = y - x;
                push(&nums, res);
                break;
            case '*':
                res = y * x;
                push(&nums, res);
                break;
            case '/':
                res = y / x;
                push(&nums, res);
                break;
            case '%':
                res = fmod(y, x);
                push(&nums, res);
                break;
            default:
		pushc(&sym, temp);
                break;
        }
}
token mygetnex(char *str, int *restart){
	static int i = 0;
	static int ind = 1;
	int j = 0;
	token t;
	char temp[128], tri[5]; 
	if(*restart == 1)	{
		i = 0;
		*restart = 0;	
	}
	while(1){
		if(str[i] != ' ')
			break;
		i++;	
	}
	strncpy(tri, &str[i], 3);
	if(strcmp(tri, "sqt") == 0){
		t.data.op = 'q';
		t.type = OPERATOR;
		ind = 1;
		i = i + 3;
		return t;	
	}
	else if(strcmp(tri, "sin") == 0){
		t.data.op = 's';
		t.type = OPERATOR;
		ind = 1;
		i = i + 3;
		return t;	
	}
	else if(strcmp(tri, "cos") == 0){
		t.data.op = 'c';
		t.type = OPERATOR;
		ind = 1;
		i = i + 3;
		return t;	
	}
	else if(strcmp(tri, "tan") == 0){
		t.data.op = 't';
		t.type = OPERATOR;
		ind = 1;
		i = i + 3;
		return t;	
	}
	else if(strcmp(tri, "log") == 0){
		t.data.op = 'l';
		t.type = OPERATOR;
		ind = 1;
		i = i + 3;
		return t;	
	}
	else if(strcmp(tri, "inv") == 0){
		t.data.op = 'i';
		t.type = OPERATOR;
		ind = 1;
		i = i + 3;
		return t;	
	}else if(strcmp(tri, "sqt") == 0){
		t.data.op = 'q';
		t.type = OPERATOR;
		ind = 1;
		i = i + 3;
		return t;	
	}else if(str[i] >= '0' && str[i] <= '9'){
		while(1){
			if(!(str[i] >= '0' && str[i] <= '9') && str[i] != '.')
				break;
			temp[j] = str[i];
			i++;
			j++;
		}
		temp[j] = '\0';
		t.data.num = atof(temp);
		t.type = OPERAND;
		ind = 0;
		return t;
	}
	else if(str[i] == '-' && (ind == 1)){

			temp[j++] = str[i++];
			while(1){
				if(!(str[i] >= '0' && str[i] <= '9') && str[i] != '.')
					break;
				temp[j] = str[i];
				i++;
				j++;
			}
			temp[j] = '\0';
			t.data.num = atof(temp);
			t.type = OPERAND;
			ind = 0;
			return t;	
	}
	else if(str[i] == '\0'){
		t.type = END;
		return t;
	}
	else{
		switch(str[i]){
			case '+': case '-': case '*': case '%': 
			case '(': case ')': case '/': case '^':
			case '!':
				t.data.op = str[i];
				t.type = OPERATOR;
				i++;
				ind = 1;
				return t;
				break;
			default:
				t.type = ERROR;
				i++;
				ind = 1;
				return t;
				break;	 
		}
	}
		
	
	
		
}

double infix(char *str) {
    	char ch, ch1;
	int restart = 1;
	init(&nums);
	initc(&sym);
	token t;
	while(1) {
	   t = mygetnex(str, &restart);
	   if(t.type == OPERAND)
		push(&nums, t.data.num);
	   else if(t.type == OPERATOR) {
		if(isemptyc(&sym))
                	pushc(&sym, t.data.op);
            	else    {
                  ch = popc(&sym);
                  switch(t.data.op) {
                    case ')':
			pushc(&sym, ch);
                        while(1)	{
			    ch1 = popc(&sym);
			    if(ch1 != '(')
                            	process(ch1);
			    else
				break;
			}
                        break;
                    case '(':
			pushc(&sym, ch);
			pushc(&sym, t.data.op);
                        break;
                    case '+':
                    case '-':
                        if(ch == '(')
				pushc(&sym, ch);
                        else
				process(ch);
			pushc(&sym, t.data.op);
                        break;
                    case 's':
                    case 'c':
                    case 't':
                    case 'l':
                    case 'i':
                    case 'q':
                            if(ch == '^' || ch == '%' || ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(')   {
                            pushc(&sym, ch);
                            pushc(&sym, t.data.op);
                        }
                        else    {
                            process(ch);
			    pushc(&sym, t.data.op);
                        }
                            break;                        
                    case '*':
                    case '/':
                        if(ch == '+' || ch == '-' || ch == '(')   {
                            pushc(&sym, ch);
                            pushc(&sym, t.data.op);
                        }
                        else    {
                            process(ch);
			    pushc(&sym, t.data.op);
                        }
                        break;
		    case '^':
                        if(ch == '%' || ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(')   {
                            pushc(&sym, ch);
                            pushc(&sym, t.data.op);
                        }
                        else    {
                            process(ch);
			    pushc(&sym, t.data.op);
                        }
                        break;
                    case '!':
                        if(ch == '^' || ch == '%' || ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(')   {
                            pushc(&sym, ch);
                            pushc(&sym, t.data.op);
                        }
                        else    {
                            process(ch);
			    pushc(&sym, t.data.op);
                        }
                        break;
                    case '%':
                        if(ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(')   {
                            pushc(&sym, ch);
                            pushc(&sym, t.data.op);
                        }
                        else    {
                            process(ch);
			    pushc(&sym, t.data.op);
                        }
                        break;
                    default:
			pushc(&sym, ch);
                        ch1 = popc(&sym);
                        process(ch1);
			break;
                  }
                }
	   }else if(t.type == END) 
		break;
	    else if(t.type == ERROR)
	    	return INT_MIN;
	}
	while(!isemptyc(&sym))    {
                ch1 = popc(&sym);
        	process(ch1);
	}
	if(isemptyc(&sym) && islone(&nums))       
		return pop(&nums);
	else	
		return INT_MIN;
}

/*
	The functions that the buttons will perform are written from 		here on.
*/	

G_MODULE_EXPORT void c_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	if(str[strlen(str) - 1] == ' ')
		str[strlen(str) - 3] = 0;
	else
		str[strlen(str) - 1] = 0;
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void clr_pressed(GtkButton *button, textBox *widg){
	gtk_entry_set_text (widg->e2, "");
	gtk_entry_set_text (widg->e1, "");
}
G_MODULE_EXPORT void equal_pressed(GtkButton *button, textBox *widg){
	char str[128];
	static int refresh = 0;
	char ans[32];
	static double y;
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	str[strlen(str)] = '\0';
	y = infix(str);
	if(refresh == 0){	
		if(y == INT_MIN){
			refresh = 1;
			g_print("Reevaluating\n");
			equal_pressed(button, widg);
		}
	}	
	if(refresh == 0){
		if(y == INT_MIN)
			gtk_entry_set_text (widg->e2, "Error in Expression");
		else{
			sprintf(ans, "%lf", (double) y);
			gtk_entry_set_text (widg->e2, ans);
		}
	}
	refresh = 0;	
}
G_MODULE_EXPORT void pi_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "3.14159265");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void euler_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "2.718281828");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void mem_pressed(GtkButton *button, textBox *widg){
	static char disp[128];
	char str1[128], str2[128];
	const gchar *text2 = gtk_entry_get_text(widg->e2);
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str1, text1);
	strcpy(str2, text2);
	if(strlen(str2) != 0)
		strcpy(disp, str2);
	else{
		strcat(str1, disp);
		gtk_entry_set_text (widg->e1, str1);
	}
}
G_MODULE_EXPORT void plus_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "+");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void minus_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "-");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void mul_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "*");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void div_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "/");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void mod_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "%");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void fac_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "!");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void dec_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, ".");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void par1_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "(");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void par2_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, ")");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void pow_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "^");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void one_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "1");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void two_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "2");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void three_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "3");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void four_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "4");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void five_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "5");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void six_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "6");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void seven_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "7");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void eight_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "8");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void nine_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "9");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void zero_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "0");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void sin_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "sin(");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void cos_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "cos(");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void tan_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "tan(");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void log_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "log(");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void inv_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "inv(");
	gtk_entry_set_text (widg->e1, str);
}
G_MODULE_EXPORT void sqrt_pressed(GtkButton *button, textBox *widg){
	char str[128];
	const gchar *text1 = gtk_entry_get_text(widg->e1);
	strcpy(str, text1);
	strcat(str, "sqt(");
	gtk_entry_set_text (widg->e1, str);
}

/*
	The Gtk main program which triggers the Gtk loop.
*/
int main(int argc, char **argv)
{
    	GtkBuilder *builder;
    	GtkWidget  *window;
    	GError     *error = NULL;
	textBox widg;
    	gtk_init( &argc, &argv );

 
    	builder = gtk_builder_new();

	gtk_builder_add_from_file(builder, "calgra.xml", &error);
    	window = GTK_WIDGET( gtk_builder_get_object(builder, "window1"));
	widg.e1 = GTK_ENTRY(gtk_builder_get_object(builder, "entry1"));		// The corresponding GtkWidgets and Windows
	widg.e2 = GTK_ENTRY(gtk_builder_get_object(builder, "entry2"));		// from calgra.xmal file
        gtk_builder_connect_signals( builder, &widg );				// Connecting the signals. This line connects
										// signals of all widgets as they are in window itself.
    	g_object_unref(G_OBJECT(builder));

	gtk_widget_show(window);
	gtk_main();

    	return 0;
}
