//#define MAX 64



typedef struct stack{

    double a[32];

    int i;

}stack;

typedef struct stackc{

    char a[32];

    int i;

}stackc;

void push(stack *s, double num);

double pop(stack *s);

void init(stack *s);

int isempty(stack *s);

int isfull(stack *s);

int islone(stack *s);


void pushc(stackc *s, char num);

char popc(stackc *s);

void initc(stackc *s);

int isemptyc(stackc *s);

int isfullc(stackc *s);
